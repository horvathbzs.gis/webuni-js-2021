const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('Promise timeout has run');
    resolve('foo');
    reject('hello');
  }, 300);
});

async function testAwait() {
  try {
    const value = await myPromise;
    console.log(`Await success: ${value}`);
  } catch (error) {
    console.log(`Await error: ${error}`);
  }
}

testAwait();

myPromise
  .then((value) => console.log(`Success: ${value}`))
  .catch((error) => console.log(`Error: ${error}`));