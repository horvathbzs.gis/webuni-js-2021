const testRequest = async () => {
  const response = await fetch('https://api.genderize.io/?name=Peter', {
    method: 'GET',
  });

  console.log(await response.json());
};

testRequest();

const testPostRequest = async () => {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: 'foo',
      body: 'bar',
      userId: 1,
      isBodey: true,
    }),
    headers: {
      'Content-type': 'application/json',
    },
  });

  console.log('testPostRequest()');
  console.log(await response.json());
};

testPostRequest();

fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  body: JSON.stringify({
    title: 'foo',
    body: 'bar',
    userId: 1,
    isBodey: true,
  }),
  headers: {
    'Content-type': 'application/json',
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));
